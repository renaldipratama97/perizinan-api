import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import multer from 'multer';
import * as cron from 'node-cron';

import { fileURLToPath } from 'url';
import path from 'path';
import db from './src/config/db.js';
import pdf from 'html-pdf';
import RentTransactionReport from './src/docs/RentTransactionReport.js';

import FacilityRouter from './src/routers/FacilityRouter.js';
import SpesificationRouter from './src/routers/SpesificationRouter.js';
import BrosurRouter from './src/routers/BrosurRouter.js';
import BookingRouter from './src/routers/BookingRouter.js';
import EmployeeRouter from './src/routers/EmployeeRouter.js';
import UserRouter from './src/routers/UserRouter.js';
import LocationRouter from './src/routers/LocationRouter.js';
import AuthRouter from './src/routers/AuthRouter.js';
import PropertyRouter from './src/routers/PropertyRouter.js';
import TransaksiRouter from './src/routers/TransaksiRouter.js';
import GlobalRouter from './src/routers/GlobalRouter.js';

// models
// import Facility from "./src/models/FacilityModel.js";
// import Spesifikasi from "./src/models/SpesifikasiModel.js";
// import Brosur from "./src/models/BrosurModel.js";
// import Karyawan from "./src/models/KaryawanModel.js";
// import User from "./src/models/UserModel.js";
// import Location from './src/models/LocationModel.js';
// import Property from './src/models/PropertyModel.js';
// import PropertyFacility from "./src/models/PropertyFacilityModel.js";
// import PropertySpesification from "./src/models/PropertySpesificationModel.js";
// import PropertyPicture from "./src/models/PropertyPictureModel.js";
// import TransaksiSewa from './src/models/TransaksiSewaModel.js';
// import Booking from './src/models/BookingModel.js';
dotenv.config();
const app = express();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

try {
  db.authenticate();
  console.log('db connected');
  // Facility.sync();
  // Spesifikasi.sync();
  // Brosur.sync();
  // Karyawan.sync();
  // User.sync();
  // Location.sync();
  // Property.sync();
  // PropertyFacility.sync();
  // PropertySpesification.sync();
  // PropertyPicture.sync();
  // TransaksiSewa.sync();
  // Booking.sync();
} catch (error) {
  console.log(error);
}
app.use(cors());
app.use(express.json());
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'documents');
  },
  filename: (req, file, cb) => {
    cb(null, new Date().getTime() + '-' + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpeg' ||
    file.mimetype === 'application/pdf'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('file')
);

//Static router for image
app.use('/documents', express.static(path.join(__dirname, 'documents')));

app.post('/global/create-pdf-transaction', async (req, res) => {
  const transactions = await db.query(
    `SELECT * FROM transaksi_sewa a, property b WHERE a.kode_property=b.kode_property`
  );

  pdf
    .create(RentTransactionReport(transactions[0]), {})
    .toFile('result.pdf', (err) => {
      if (err) {
        res.send(Promise.reject());
      }

      res.send(Promise.resolve());
    });
});
app.get('/global/fetch-pdf', (req, res) => {
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = path.dirname(__filename);
  res.sendFile(`${path.join(__dirname)}/result.pdf`);
});

app.use(FacilityRouter);
app.use(SpesificationRouter);
app.use(BrosurRouter);
app.use(BookingRouter);
app.use(EmployeeRouter);
app.use(UserRouter);
app.use(AuthRouter);
app.use(PropertyRouter);
app.use(TransaksiRouter);
app.use(GlobalRouter);
app.use(LocationRouter);

app.listen(2500, () => console.log('server running in port 2500'));

// cron.schedule('59 23 * * *', () => {
//   console.log('success send email');
// });
