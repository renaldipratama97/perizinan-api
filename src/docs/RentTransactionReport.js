export default (value) => {
  return `
      <!DOCTYPE html>
      <html lang="en">
      
      <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="preconnect" href="https://fonts.googleapis.com">
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
          <title>Laporan Keuangan Project</title>
          <style>
              @import url('https://fonts.googleapis.com/css2?family=Reggae+One&display=swap');
      
              .logo {
                  font-family: 'Reggae One', cursive;
              }
      
              .container {
                  width: 600px;
                  margin: 0 auto;
              }
      
              .header-title {
                  font-size: 25px;
                  width: 60%;
                  margin: 0 auto;
                  text-align: center;
                  margin-top: 20px;
              }
      
      
              .header-subtitle {
                  font-size: 15px;
                  width: 100%;
                  text-align: center;
              }
      
              table {
                  width: 100%;
                  margin-top: 30px;
              }
      
              table, thead, tbody, th, td {
                  border: 1px solid rgb(166, 166, 166);
              }
      
              tr, td {
                  text-align: center;
              }
          </style>
      </head>
      
      <body>
          <div class="container">
              <div class="logo header-title">PT Aricha Propertindo
              </div>
              <div class="header-subtitle">[ Laporan Transaksi Sewa Property ]</div>
      
              <table class="min-w-full">
                  <thead class="border-b">
                      <tr>
                          <th scope="col">
                              No
                          </th>
                          <th scope="col">
                              Nama Property
                          </th>
                          <th scope="col">
                              Jenis Pembayaran
                          </th>
                          <th scope="col">
                              Total Bayar
                          </th>
                          <th scope="col">
                              Nama Lengkap
                          </th>
                          <th scope="col">
                              No Handphone
                          </th>
                      </tr>
                  </thead>
                  <tbody>
                  ${value
                    .map(
                      (val, index) => `<tr>
                          <td>${index + 1}</td>
                          <td>
                            ${val.nama_property}
                          </td>
                          <td>
                            Rp ${val.jenis_pembayaran}
                          </td>
                          <td>
                            Rp ${val.total_bayar.toLocaleString('en-US')}
                          </td>
                          <td>
                            ${val.nama_lengkap}
                          </td>
                          <td>
                            ${val.no_handphone}
                          </td>
                      </tr>`
                    )
                    .join('')}
                  </tbody>
              </table>
          </div>
      </body>
      
      </html>`;
};
