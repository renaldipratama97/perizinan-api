import express from "express";
import {
  getSpesifications,
  getSpesificationByID,
  saveSpesification,
  updateSpesification,
  deleteSpesification,
} from "../controllers/SpesificationController.js";

const router = express.Router();

router.get("/spesifications", getSpesifications);
router.get("/spesification/:kode_spesifikasi", getSpesificationByID);
router.post("/spesification", saveSpesification);
router.put("/spesification", updateSpesification);
router.delete("/spesification/:kode_spesifikasi", deleteSpesification);

export default router;
