import express from "express";
import {
  getEmployees,
  getEmployeeByID,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
} from "../controllers/EmployeeController.js";

const router = express.Router();

router.get("/employees", getEmployees);
router.get("/employee/:kode_karyawan", getEmployeeByID);
router.post("/employee", saveEmployee);
router.put("/employee", updateEmployee);
router.delete("/employee/:kode_karyawan", deleteEmployee);

export default router;
