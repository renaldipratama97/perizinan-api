import express from 'express';
import {
  getLocations,
  getLocationByID,
  saveLocation,
  updateLocation,
  deleteLocation,
} from '../controllers/LocationController.js';

const router = express.Router();

router.get('/locations', getLocations);
router.get('/location/:kode_lokasi', getLocationByID);
router.post('/location', saveLocation);
router.put('/location', updateLocation);
router.delete('/location/:kode_lokasi', deleteLocation);

export default router;
