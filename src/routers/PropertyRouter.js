import express from 'express';
import {
  saveProperty,
  getPropertyByID,
  getProperties,
  updateProperty,
} from '../controllers/PropertyController.js';

const router = express.Router();

router.get('/property', getProperties);
router.get('/property/:id', getPropertyByID);
router.post('/property', saveProperty);
router.put('/property', updateProperty);

export default router;
