import express from "express";
import { getCountAllTable } from "../controllers/GlobalController.js";

const router = express.Router();

router.get("/global/count", getCountAllTable);

export default router;
