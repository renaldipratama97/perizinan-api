import express from "express";
import {
  getUsers,
  getUserByID,
  saveUser,
  deleteUser,
} from "../controllers/UserController.js";

const router = express.Router();

router.get("/users", getUsers);
router.get("/user/:id", getUserByID);
router.post("/user", saveUser);
router.delete("/user/:kode_user", deleteUser);

export default router;
