import express from 'express';
import {
  getBookings,
  saveBooking,
  updateStatusBooking,
} from '../controllers/BookingController.js';

const router = express.Router();

router.get('/bookings', getBookings);
router.post('/booking', saveBooking);
router.put('/booking', updateStatusBooking);

export default router;
