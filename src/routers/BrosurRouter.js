import express from 'express';
import {
  getBrosurs,
  getBrosurActive,
  getBrosurByID,
  saveBrosur,
  updateBrosur,
  updateStatusBrosur,
  deleteBrosur,
} from '../controllers/BrosurController.js';

const router = express.Router();

router.get('/brosurs', getBrosurs);
router.get('/brosur/active', getBrosurActive);
router.get('/brosur/:kode_brosur', getBrosurByID);
router.post('/brosur', saveBrosur);
router.put('/brosur', updateBrosur);
router.put('/brosur/change-status', updateStatusBrosur);
router.delete('/brosur/:kode_brosur', deleteBrosur);

export default router;
