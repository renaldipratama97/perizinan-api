import express from "express";
import {
  getFacilities,
  getFacilityByID,
  saveFacility,
  updateFacility,
  deleteFacility,
} from "../controllers/FacilityController.js";

const router = express.Router();

router.get("/facilities", getFacilities);
router.get("/facility/:kode_fasilitas", getFacilityByID);
router.post("/facility", saveFacility);
router.put("/facility", updateFacility);
router.delete("/facility/:kode_fasilitas", deleteFacility);

export default router;
