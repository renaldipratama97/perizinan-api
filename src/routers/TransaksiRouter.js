import express from 'express';
import {
  getTransaksiSewa,
  saveTransaksiSewa,
} from '../controllers/TransaksiSewaController.js';

const router = express.Router();

router.get('/transaksi-sewa', getTransaksiSewa);
router.post('/transaksi-sewa', saveTransaksiSewa);

export default router;
