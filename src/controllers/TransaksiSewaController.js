import TransaksiSewa from '../models/TransaksiSewaModel.js';
import Property from '../models/PropertyModel.js';
import { v4 as uuidv4 } from 'uuid';
import db from '../config/db.js';

export const getTransaksiSewa = async (req, res) => {
  try {
    const transactions = await db.query(
      `SELECT * FROM transaksi_sewa a, property b, karyawan c WHERE a.kode_property=b.kode_property AND a.kode_employee=c.kode_karyawan`
    );
    res.json({ statusCode: 200, message: 'success', data: transactions[0] });
  } catch (error) {
    console.log(error);
  }
};

export const saveTransaksiSewa = async (req, res) => {
  if (!req.file) {
    return res.json({
      statusCode: 422,
      message: 'File pendukung harus di Upload',
    });
  }

  const {
    kode_property,
    jenis_pembayaran,
    kode_employee,
    jatuh_tempo,
    total_bayar,
    nik,
    nama_lengkap,
    email,
    no_handphone,
    alamat_lengkap,
  } = req.body;

  try {
    const property = await Property.findOne({ where: { kode_property } });

    await TransaksiSewa.create({
      kode_transaksi_sewa: uuidv4(),
      kode_property,
      jenis_pembayaran,
      kode_employee,
      jatuh_tempo,
      total_bayar,
      nik,
      nama_lengkap,
      email,
      no_handphone,
      alamat_lengkap,
      file_pendukung: req.file.path,
    });

    await Property.update(
      {
        stok: property.stok - 1,
      },
      {
        where: {
          kode_property,
        },
      }
    );
    res.json({ statusCode: 201, message: 'success simpan data transaksi' });
  } catch (error) {
    console.log(error);
  }
};
