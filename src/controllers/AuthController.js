import bcrypt from "bcrypt";
import User from "../models/UserModel.js";
import Employee from "../models/KaryawanModel.js";
import jwt from "jsonwebtoken";

export const signin = async (req, res) => {
  const { username, password } = req.body;

  // Check for user username
  const user = await User.findOne({ where: { username: username } });

  if (!user) {
    return res
      .status(200)
      .json({ statusCode: 404, message: "username tidak ada di database" });
  }

  if (user) {
    const match = await bcrypt.compare(password, user.password);

    if (!match) {
      return res
        .status(200)
        .json({ statusCode: 400, message: "password salah!!!" });
    }

    const employee = await Employee.findOne({
      where: { kode_karyawan: user.nama_karyawan },
    });

    if (match) {
      res.json({
        statusCode: 200,
        message: "login success",
        data: {
          kode_user: user.kode_user,
          kode_karyawan: employee.kode_karyawan,
          nama_karyawan: employee.nama_karyawan,
          username: user.username,
          role_user: user.role_user,
          picture: employee.picture,
          token: generateToken(
            user.kode_user,
            user.username,
            user.nama_karyawan,
            employee.nama_karyawan,
            user.role_user
          ),
        },
      });
    } else {
      res.status(400);
      throw new Error("Invalid credentials");
    }
  }
};

const generateToken = (
  kode_user,
  username,
  kode_karyawan,
  nama_karyawan,
  role_user
) => {
  return jwt.sign(
    { kode_user, username, kode_karyawan, nama_karyawan, role_user },
    "camd843th4nrecqercfn303488545hf4fu9rfrnf4urf4r40204f402jf0rfjfr",
    {
      expiresIn: "1d",
    }
  );
};
