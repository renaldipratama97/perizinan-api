import Facility from "../models/FacilityModel.js";
import Spesification from "../models/SpesifikasiModel.js";
import Brosur from "../models/BrosurModel.js";
import Property from "../models/PropertyModel.js";

export const getCountAllTable = async (req, res) => {
  try {
    const facilities = await Facility.count();
    const spesification = await Spesification.count();
    const brosur = await Brosur.count();
    const property = await Property.count();
    res.json({
      statusCode: 200,
      message: "success",
      data: {
        facilities,
        spesification,
        brosur,
        property,
      },
    });
  } catch (error) {
    console.log(error);
  }
};
