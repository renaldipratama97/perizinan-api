import Booking from '../models/BookingModel.js';
import Property from '../models/PropertyModel.js';
import { v4 as uuidv4 } from 'uuid';

export const getBookings = async (req, res) => {
  const bookings = await Booking.findAll();
  res.json({ statusCode: 200, message: 'success', data: bookings });
};

export const saveBooking = async (req, res) => {
  if (!req.file) {
    return res.json({
      statusCode: 422,
      message: 'Document harus di Upload',
    });
  }

  const {
    kode_property,
    nik,
    nama_lengkap,
    email,
    no_handphone,
    alamat,
    tgl_akad,
    lokasi_akad,
  } = req.body;

  try {
    await Booking.create({
      kode_booking: uuidv4(),
      kode_property,
      nik,
      nama_lengkap,
      email,
      no_handphone,
      alamat,
      tgl_akad,
      lokasi_akad,
      document: req.file.path,
      status: 'pending',
    });

    res.json({ statusCode: 201, message: 'Booking success' });
  } catch (error) {
    console.log(error);
  }
};

export const updateStatusBooking = async (req, res) => {
  const { kode_booking, kode_property, status } = req.body;

  try {
    const property = await Property.findOne({ where: { kode_property } });

    if (!property) {
      return res.json({
        statusCode: 404,
        message: 'Property not found!',
      });
    }

    await Booking.update(
      {
        status,
      },
      {
        where: {
          kode_booking,
        },
      }
    );

    if (status === 'terjual') {
      await Property.update(
        {
          stok: property.stok - 1,
        },
        {
          where: {
            kode_property,
          },
        }
      );
    }

    res.json({ statusCode: 200, message: 'Update status success' });
  } catch (error) {
    console.log(error);
  }
};
