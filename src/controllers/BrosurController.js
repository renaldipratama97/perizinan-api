import Brosur from '../models/BrosurModel.js';
import { v4 as uuidv4 } from 'uuid';

export const getBrosurs = async (req, res) => {
  try {
    const brosurs = await Brosur.findAll();
    res.json({ statusCode: 200, message: 'success', data: brosurs });
  } catch (error) {
    console.log(error);
  }
};

export const getBrosurActive = async (req, res) => {
  try {
    const brosur = await Brosur.findAll({
      where: {
        status_brosur: 1,
      },
    });
    res.json({ statusCode: 200, message: 'success', data: brosur });
  } catch (error) {
    console.log(error);
  }
};

export const getBrosurByID = async (req, res) => {
  try {
    const { kode_brosur } = req.params;
    const brosur = await Brosur.findOne({
      where: { kode_brosur },
    });

    if (!brosur) {
      return res.json({ statusCode: 404, message: 'brosur not found' });
    }

    res.json({ statusCode: 200, message: 'success', data: brosur });
  } catch (error) {
    console.log(error);
  }
};

export const saveBrosur = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: 'File harus di Upload',
      });
    }

    const { nama_brosur } = req.body;

    await Brosur.create({
      kode_brosur: uuidv4(),
      nama_brosur,
      url_brosur: req.file.path,
      status_brosur: 1,
    });
    res.json({ statusCode: 201, message: 'success simpan data brosur' });
  } catch (error) {
    console.log(error);
  }
};

export const updateBrosur = async (req, res) => {
  const { kode_brosur, nama_brosur } = req.body;

  try {
    if (!req.file) {
      await Brosur.update(
        {
          nama_brosur,
        },
        {
          where: {
            kode_brosur,
          },
        }
      );
    } else {
      await Brosur.update(
        {
          nama_brosur,
          url_brosur: req.file.path,
        },
        {
          where: {
            kode_brosur,
          },
        }
      );
    }
    res.json({ statusCode: 200, message: 'success update data brosur' });
  } catch (error) {
    console.log(error);
  }
};

export const updateStatusBrosur = async (req, res) => {
  const { kode_brosur, status } = req.body;

  try {
    await Brosur.update(
      {
        status_brosur: status,
      },
      {
        where: {
          kode_brosur,
        },
      }
    );
    res.json({ statusCode: 200, message: 'success update status brosur' });
  } catch (error) {
    console.log(error);
  }
};

export const deleteBrosur = async (req, res) => {
  try {
    const { kode_brosur } = req.params;
    const deleteBrosur = Brosur.destroy({
      where: {
        kode_brosur,
      },
    });

    if (deleteBrosur) {
      res.json({ statusCode: 200, message: 'success delete data brosur' });
    }
  } catch (error) {
    console.log(error);
  }
};
