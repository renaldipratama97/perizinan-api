import { v4 as uuidv4 } from "uuid";
import bcrypt from "bcrypt";
import User from "../models/UserModel.js";
import Employee from "../models/KaryawanModel.js";
import db from "../config/db.js";

export const getUsers = async (req, res) => {
  try {
    const users = await db.query(
      `SELECT * FROM user a, karyawan b WHERE a.nama_karyawan=b.kode_karyawan`
    );
    res.json({ statusCode: 200, message: "success", data: users[0] });
  } catch (error) {
    console.log(error);
  }
};

export const getUserByID = async (req, res) => {
  try {
    const id = req.params.id;
    const user = await User.findOne({
      where: { kode_user: id },
    });

    if (!user) {
      return res.json({ statusCode: 404, message: "error" });
    }

    res.json({ statusCode: 200, message: "success", data: user });
  } catch (error) {
    console.log(error);
  }
};

export const saveUser = async (req, res) => {
  try {
    const { karyawan, username, password, role_user } = req.body;

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const getEmployee = await User.findAll({
      where: { nama_karyawan: karyawan },
    });

    if (getEmployee.length) {
      return res.json({
        statusCode: 999,
        message: "karyawan sudah didaftarkan di table user...",
      });
    }

    await User.create({
      kode_user: uuidv4(),
      nama_karyawan: karyawan,
      username,
      password: hashPassword,
      role_user,
    });
    res.json({ statusCode: 201, message: "success simpan data user" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteUser = async (req, res) => {
  try {
    const { kode_user } = req.params;
    const deleteUser = User.destroy({
      where: {
        kode_user,
      },
    });

    if (deleteUser) {
      res.json({ statusCode: 200, message: "success delete data user" });
    }
  } catch (error) {
    console.log(error);
  }
};
