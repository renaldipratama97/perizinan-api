import Property from '../models/PropertyModel.js';
import PropertyPicture from '../models/PropertyPictureModel.js';
import PropertyFacility from '../models/PropertyFacilityModel.js';
import PropertySpesification from '../models/PropertySpesificationModel.js';
import Facility from '../models/FacilityModel.js';
import Spesification from '../models/SpesifikasiModel.js';
import { v4 as uuidv4 } from 'uuid';
import db from '../config/db.js';

export const getProperties = async (req, res) => {
  const propertyData = [];
  const properties = await Property.findAll();

  for (const property of properties) {
    // const propertyFacility = await db.query(
    //   `SELECT * FROM property_facility a, fasilitas b WHERE a.kode_property=${property.kode_property} AND a.kode_facility=b.kode_fasilitas`
    // );
    const propertyFacility = await PropertyFacility.findAll({
      where: {
        kode_property: property.kode_property,
      },
    });

    // const propertySpesification = await db.query(
    //   `SELECT * FROM property_spesification a, spesifikasi b WHERE a.kode_property=${property.kode_property} AND a.kode_spesification=b.kode_spesifikasi`
    // );

    const propertySpesification = await PropertySpesification.findAll({
      where: {
        kode_property: property.kode_property,
      },
    });

    const propertyImages = await PropertyPicture.findAll({
      where: {
        kode_property: property.kode_property,
      },
    });

    propertyData.push({
      property: property,
      facility: propertyFacility,
      spesification: propertySpesification,
      picture: propertyImages,
    });
  }
  res.json({ statusCode: 200, message: 'success', data: propertyData });
};

export const getPropertyByID = async (req, res) => {
  const { id } = req.params;
  const property = await Property.findOne({ where: { kode_property: id } });

  if (!property) {
    res.json({ statusCode: 404, message: 'Property not found!' });
  }

  // const propertyFacility = await PropertyFacility.findAll({
  //   where: {
  //     kode_property: property.kode_property,
  //   },
  // });

  const propertyFacility = await db.query(
    `SELECT * FROM property_facility a, fasilitas b WHERE a.kode_property='${property.kode_property}' AND a.kode_facility=b.kode_fasilitas`
  );

  // const propertySpesification = await PropertySpesification.findAll({
  //   where: {
  //     kode_property: property.kode_property,
  //   },
  // });

  const propertySpesification = await db.query(
    `SELECT * FROM property_spesification a, spesifikasi b WHERE a.kode_property='${property.kode_property}' AND a.kode_spesification=b.kode_spesifikasi`
  );

  const propertyImages = await PropertyPicture.findAll({
    where: {
      kode_property: property.kode_property,
    },
  });

  const propertyData = {
    property: property,
    facility: propertyFacility[0],
    spesification: propertySpesification[0],
    picture: propertyImages,
  };

  // console.log(propertyData);
  res.json({ statusCode: 200, message: 'success', data: propertyData });
};

export const saveProperty = async (req, res) => {
  if (!req.file) {
    return res.json({
      statusCode: 422,
      message: 'Property picture harus di Upload',
    });
  }

  const {
    nama_property,
    type_property,
    stock_property,
    latitude,
    longitude,
    alamat_property,
    harga_property,
    harga_sewa_property,
    fasilitas,
    spesifikasi,
  } = req.body;

  try {
    const propertyCode = uuidv4();
    const facilityParse = JSON.parse(fasilitas);
    const spesificationParse = JSON.parse(spesifikasi);

    await Property.create({
      kode_property: propertyCode,
      nama_property,
      type_property,
      stok: stock_property,
      latitude,
      longitude,
      alamat: alamat_property,
      harga_property,
      harga_sewa_property,
    });
    const getFacilities = await Facility.findAll();
    const getSpesification = await Spesification.findAll();

    getFacilities.forEach((val, index) => {
      PropertyFacility.create({
        kode_property_facility: uuidv4(),
        kode_property: propertyCode,
        kode_facility: val.kode_fasilitas,
        value: facilityParse[index],
      });
    });

    getSpesification.forEach((val, index) => {
      PropertySpesification.create({
        kode_property_spesification: uuidv4(),
        kode_property: propertyCode,
        kode_spesification: val.kode_spesifikasi,
        value: spesificationParse[index],
      });
    });

    await PropertyPicture.create({
      kode_property_picture: uuidv4(),
      kode_property: propertyCode,
      url_picture: req.file.path,
    });
    res.json({ statusCode: 201, message: 'success simpan data property' });
  } catch (error) {
    console.log(error);
  }
};

export const updateProperty = async (req, res) => {
  const {
    kode_property,
    nama_property,
    type_property,
    stock_property,
    latitude,
    longitude,
    alamat,
    harga_property,
    harga_sewa_property,
  } = req.body;

  try {
    await Property.update(
      {
        nama_property,
        type_property,
        stok: stock_property,
        latitude,
        longitude,
        alamat,
        harga_property,
        harga_sewa_property,
      },
      {
        where: {
          kode_property,
        },
      }
    );

    res.json({ statusCode: 200, message: 'success update data property' });
  } catch (error) {
    console.log(error);
  }
};
