import Facility from "../models/FacilityModel.js";
import { v4 as uuidv4 } from "uuid";

export const getFacilities = async (req, res) => {
  try {
    const facilities = await Facility.findAll();
    res.json({ statusCode: 200, message: "success", data: facilities });
  } catch (error) {
    console.log(error);
  }
};

export const getFacilityByID = async (req, res) => {
  try {
    const { kode_fasilitas } = req.params;
    const facility = await Facility.findOne({
      where: { kode_fasilitas },
    });

    if (!facility) {
      return res.json({ statusCode: 404, message: "facility not found" });
    }

    res.json({ statusCode: 200, message: "success", data: facility });
  } catch (error) {
    console.log(error);
  }
};

export const saveFacility = async (req, res) => {
  const { nama_fasilitas } = req.body;

  try {
    await Facility.create({
      kode_fasilitas: uuidv4(),
      nama_fasilitas,
    });
    res.json({ statusCode: 201, message: "success simpan data fasilitas" });
  } catch (error) {
    console.log(error);
  }
};

export const updateFacility = async (req, res) => {
  const { kode_fasilitas, nama_fasilitas } = req.body;

  try {
    await Facility.update(
      {
        nama_fasilitas,
      },
      {
        where: {
          kode_fasilitas,
        },
      }
    );
    res.json({ statusCode: 200, message: "success update data fasilitas" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteFacility = async (req, res) => {
  try {
    const { kode_fasilitas } = req.params;
    const deleteFacility = Facility.destroy({
      where: {
        kode_fasilitas,
      },
    });

    if (deleteFacility) {
      res.json({ statusCode: 200, message: "success delete data fasilitas" });
    }
  } catch (error) {
    console.log(error);
  }
};
