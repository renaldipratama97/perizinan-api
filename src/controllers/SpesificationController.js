import Spesification from "../models/SpesifikasiModel.js";
import { v4 as uuidv4 } from "uuid";

export const getSpesifications = async (req, res) => {
  try {
    const spesifications = await Spesification.findAll();
    res.json({ statusCode: 200, message: "success", data: spesifications });
  } catch (error) {
    console.log(error);
  }
};

export const getSpesificationByID = async (req, res) => {
  try {
    const { kode_spesifikasi } = req.params;
    const spesification = await Spesification.findOne({
      where: { kode_spesifikasi },
    });

    if (!spesification) {
      return res.json({ statusCode: 404, message: "spesification not found" });
    }

    res.json({ statusCode: 200, message: "success", data: spesification });
  } catch (error) {
    console.log(error);
  }
};

export const saveSpesification = async (req, res) => {
  const { nama_spesifikasi } = req.body;

  try {
    await Spesification.create({
      kode_spesifikasi: uuidv4(),
      nama_spesifikasi,
    });
    res.json({ statusCode: 201, message: "success simpan data spesifikasi" });
  } catch (error) {
    console.log(error);
  }
};

export const updateSpesification = async (req, res) => {
  const { kode_spesifikasi, nama_spesifikasi } = req.body;

  try {
    await Spesification.update(
      {
        nama_spesifikasi,
      },
      {
        where: {
          kode_spesifikasi,
        },
      }
    );
    res.json({ statusCode: 200, message: "success update data spesifikasi" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteSpesification = async (req, res) => {
  try {
    const { kode_spesifikasi } = req.params;
    const deleteSpesification = Spesification.destroy({
      where: {
        kode_spesifikasi,
      },
    });

    if (deleteSpesification) {
      res.json({ statusCode: 200, message: "success delete data spesifikasi" });
    }
  } catch (error) {
    console.log(error);
  }
};
