import Employee from '../models/KaryawanModel.js';
import { v4 as uuidv4 } from 'uuid';

export const getEmployees = async (req, res) => {
  try {
    const employees = await Employee.findAll();
    res.json({ statusCode: 200, message: 'success', data: employees });
  } catch (error) {
    console.log(error);
  }
};

export const getEmployeeByID = async (req, res) => {
  try {
    const { kode_karyawan } = req.params;
    const employee = await Employee.findOne({
      where: { kode_karyawan },
    });

    if (!employee) {
      return res.json({ statusCode: 404, message: 'employee not found' });
    }

    res.json({ statusCode: 200, message: 'success', data: employee });
  } catch (error) {
    console.log(error);
  }
};

export const saveEmployee = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: 'File harus di Upload',
      });
    }

    const {
      nik,
      nama_karyawan,
      tempat_lahir,
      tgl_lahir,
      jenis_kelamin,
      jabatan,
      no_handphone,
      alamat_lengkap,
    } = req.body;

    const getEmployee = await Employee.findAll({ where: { nik: nik } });

    if (getEmployee.length) {
      return res.json({
        statusCode: 999,
        message: 'nik sudah digunakan oleh karyawan lain...',
      });
    }

    await Employee.create({
      kode_karyawan: uuidv4(),
      nik,
      nama_karyawan,
      tempat_lahir,
      tgl_lahir,
      jenis_kelamin,
      jabatan,
      no_handphone,
      alamat_lengkap,
      picture: req.file.path,
    });
    res.json({ statusCode: 201, message: 'success simpan data karyawan' });
  } catch (error) {
    console.log(error);
  }
};

export const updateEmployee = async (req, res) => {
  const {
    kode_karyawan,
    nik,
    nama_karyawan,
    tempat_lahir,
    jenis_kelamin,
    jabatan,
    no_handphone,
    alamat_lengkap,
  } = req.body;

  try {
    if (!req.file) {
      await Employee.update(
        {
          nik,
          nama_karyawan,
          tempat_lahir,
          jenis_kelamin,
          jabatan,
          no_handphone,
          alamat_lengkap,
        },
        {
          where: {
            kode_karyawan,
          },
        }
      );
    } else {
      await Employee.update(
        {
          nik,
          nama_karyawan,
          tempat_lahir,
          jenis_kelamin,
          jabatan,
          no_handphone,
          alamat_lengkap,
          picture: req.file.path,
        },
        {
          where: {
            kode_karyawan,
          },
        }
      );
    }
    res.json({ statusCode: 200, message: 'success update data karyawan' });
  } catch (error) {
    console.log(error);
  }
};

export const deleteEmployee = async (req, res) => {
  try {
    const { kode_karyawan } = req.params;
    const deleteEmployee = Employee.destroy({
      where: {
        kode_karyawan,
      },
    });

    if (deleteEmployee) {
      res.json({ statusCode: 200, message: 'success delete data karyawan' });
    }
  } catch (error) {
    console.log(error);
  }
};
