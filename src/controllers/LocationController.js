import Location from '../models/LocationModel.js';
import { v4 as uuidv4 } from 'uuid';

export const getLocations = async (req, res) => {
  try {
    const locations = await Location.findAll();
    res.json({ statusCode: 200, message: 'success', data: locations });
  } catch (error) {
    console.log(error);
  }
};

export const getLocationByID = async (req, res) => {
  try {
    const { kode_lokasi } = req.params;
    const location = await Location.findOne({
      where: { kode_lokasi },
    });

    if (!location) {
      return res.json({ statusCode: 404, message: 'location not found' });
    }

    res.json({ statusCode: 200, message: 'success', data: location });
  } catch (error) {
    console.log(error);
  }
};

export const saveLocation = async (req, res) => {
  const { nama_lokasi, latitude, longitude, alamat, icon } = req.body;

  try {
    await Location.create({
      kode_lokasi: uuidv4(),
      nama_lokasi,
      latitude,
      longitude,
      alamat,
      icon,
    });
    res.json({ statusCode: 201, message: 'success simpan data lokasi' });
  } catch (error) {
    console.log(error);
  }
};

export const updateLocation = async (req, res) => {
  const { kode_lokasi, nama_lokasi, latitude, longitude, alamat, icon } =
    req.body;

  try {
    await Location.update(
      {
        nama_lokasi,
        latitude,
        longitude,
        alamat,
        icon,
      },
      {
        where: {
          kode_lokasi,
        },
      }
    );
    res.json({ statusCode: 200, message: 'success update data lokasi' });
  } catch (error) {
    console.log(error);
  }
};

export const deleteLocation = async (req, res) => {
  try {
    const { kode_lokasi } = req.params;
    const deleteLokasi = Location.destroy({
      where: {
        kode_lokasi,
      },
    });

    if (deleteLokasi) {
      res.json({ statusCode: 200, message: 'success delete data lokasi' });
    }
  } catch (error) {
    console.log(error);
  }
};
