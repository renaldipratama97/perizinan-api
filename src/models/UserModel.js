import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const User = db.define(
  "user",
  {
    kode_user: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_karyawan: {
      type: DataTypes.STRING(40),
    },
    username: {
      type: DataTypes.STRING(30),
    },
    password: {
      type: DataTypes.STRING(64),
    },
    role_user: {
      type: DataTypes.STRING(20),
    },
    status_user: {
      type: DataTypes.INTEGER(3),
    },
  },
  {
    freezeTableName: true,
  }
);
export default User;
