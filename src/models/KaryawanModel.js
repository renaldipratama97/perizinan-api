import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Karyawan = db.define(
  "karyawan",
  {
    kode_karyawan: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nik: {
      type: DataTypes.STRING(16),
    },
    nama_karyawan: {
      type: DataTypes.STRING(30),
    },
    tempat_lahir: {
      type: DataTypes.STRING(30),
    },
    tgl_lahir: {
      type: DataTypes.DATEONLY,
    },
    jenis_kelamin: {
      type: DataTypes.STRING(15),
    },
    jabatan: {
      type: DataTypes.STRING(30),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    alamat_lengkap: {
      type: DataTypes.TEXT,
    },
    picture: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Karyawan;
