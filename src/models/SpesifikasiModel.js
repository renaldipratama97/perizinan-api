import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Spesifikasi = db.define(
  "spesifikasi",
  {
    kode_spesifikasi: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_spesifikasi: {
      type: DataTypes.STRING(30),
    },
  },
  {
    freezeTableName: true,
  }
);
export default Spesifikasi;
