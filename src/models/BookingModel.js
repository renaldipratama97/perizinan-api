import { Sequelize } from 'sequelize';
import db from '../config/db.js';

const { DataTypes } = Sequelize;

const Booking = db.define(
  'booking',
  {
    kode_booking: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    kode_property: {
      type: DataTypes.STRING(40),
    },
    nik: {
      type: DataTypes.STRING(16),
    },
    nama_lengkap: {
      type: DataTypes.STRING(35),
    },
    email: {
      type: DataTypes.STRING(25),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    alamat: {
      type: DataTypes.TEXT,
    },
    document: {
      type: DataTypes.TEXT,
    },
    tgl_akad: {
      type: DataTypes.DATE,
    },
    lokasi_akad: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.STRING(20),
    },
  },
  {
    freezeTableName: true,
  }
);
export default Booking;
