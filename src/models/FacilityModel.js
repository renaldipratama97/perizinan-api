import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Fasilitas = db.define(
  "fasilitas",
  {
    kode_fasilitas: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_fasilitas: {
      type: DataTypes.STRING(30),
    },
  },
  {
    freezeTableName: true,
  }
);
export default Fasilitas;
