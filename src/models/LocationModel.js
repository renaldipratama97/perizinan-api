import { Sequelize } from 'sequelize';
import db from '../config/db.js';

const { DataTypes } = Sequelize;

const Location = db.define(
  'lokasi',
  {
    kode_lokasi: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_lokasi: {
      type: DataTypes.TEXT,
    },
    latitude: {
      type: DataTypes.TEXT,
    },
    longitude: {
      type: DataTypes.TEXT,
    },
    alamat: {
      type: DataTypes.TEXT,
    },
    icon: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Location;
