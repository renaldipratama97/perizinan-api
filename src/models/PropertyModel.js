import { Sequelize } from 'sequelize';
import db from '../config/db.js';

const { DataTypes } = Sequelize;

const Property = db.define(
  'property',
  {
    kode_property: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_property: {
      type: DataTypes.STRING(30),
    },
    type_property: {
      type: DataTypes.STRING(15),
    },
    latitude: {
      type: DataTypes.TEXT,
    },
    longitude: {
      type: DataTypes.TEXT,
    },
    alamat: {
      type: DataTypes.TEXT,
    },
    stok: {
      type: DataTypes.INTEGER(5),
    },
    harga_property: {
      type: DataTypes.INTEGER(12),
    },
    harga_sewa_property: {
      type: DataTypes.INTEGER(12),
    },
  },
  {
    freezeTableName: true,
  }
);
export default Property;
