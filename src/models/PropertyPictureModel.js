import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const PropertyPicture = db.define(
  "property_picture",
  {
    kode_property_picture: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    kode_property: {
      type: DataTypes.STRING(40),
    },
    url_picture: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default PropertyPicture;
