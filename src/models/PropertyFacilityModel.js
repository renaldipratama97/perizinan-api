import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const PropertyFacility = db.define(
  "property_facility",
  {
    kode_property_facility: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    kode_property: {
      type: DataTypes.STRING(40),
    },
    kode_facility: {
      type: DataTypes.STRING(40),
    },
    value: {
      type: DataTypes.STRING(10),
    },
  },
  {
    freezeTableName: true,
  }
);
export default PropertyFacility;
