import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const PropertySpesification = db.define(
  "property_spesification",
  {
    kode_property_spesification: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    kode_property: {
      type: DataTypes.STRING(40),
    },
    kode_spesification: {
      type: DataTypes.STRING(40),
    },
    value: {
      type: DataTypes.STRING(10),
    },
  },
  {
    freezeTableName: true,
  }
);
export default PropertySpesification;
