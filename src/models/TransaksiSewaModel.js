import { Sequelize } from 'sequelize';
import db from '../config/db.js';

const { DataTypes } = Sequelize;

const TransaksiSewa = db.define(
  'transaksi_sewa',
  {
    kode_transaksi_sewa: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    kode_property: {
      type: DataTypes.STRING(40),
    },
    kode_employee: {
      type: DataTypes.STRING(40),
    },
    jenis_pembayaran: {
      type: DataTypes.STRING(15),
    },
    jatuh_tempo: {
      type: DataTypes.DATEONLY,
    },
    total_bayar: {
      type: DataTypes.INTEGER(15),
    },
    nik: {
      type: DataTypes.STRING(16),
    },
    nama_lengkap: {
      type: DataTypes.STRING(35),
    },
    email: {
      type: DataTypes.STRING(20),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    alamat_lengkap: {
      type: DataTypes.TEXT,
    },
    file_pendukung: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default TransaksiSewa;
