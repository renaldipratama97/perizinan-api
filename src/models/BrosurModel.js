import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Brosur = db.define(
  "brosur",
  {
    kode_brosur: {
      type: DataTypes.STRING(40),
      primaryKey: true,
    },
    nama_brosur: {
      type: DataTypes.STRING(30),
    },
    status_brosur: {
      type: DataTypes.INTEGER(3),
    },
    url_brosur: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Brosur;
